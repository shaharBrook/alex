import React, {Component} from 'react';
import './app.css';
import Header from "./header/header";
import Context from "../context/context";
import ContentContainer from "./contentContainer/contentContainer";
import {getUser, isUserSaved, saveUser} from "../state/state";
import {getInitialUser} from "../context/initialContext";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: isUserSaved() ? getUser() : getInitialUser(),
            isLoggedIn: () => {
                return this.state.user.id;
            },
            setUser: (user) => {
                saveUser(user);
                this.setState({user});
            }
        };
    }

    render() {
        return (
            <Context.Provider value={this.state}>
                <div className="app">
                    <Header/>
                    <ContentContainer/>
                </div>
            </Context.Provider>
        );
    }
}

export default App;