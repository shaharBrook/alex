import React, {useContext, useEffect} from 'react';
import './content.css';
import Query from "./query/query";
import Context from "../../../context/context";
const ActionCable = require('actioncable');
const queries = [1,2,3,4];

const Content = () => {

    const {user} = useContext(Context);

    useEffect(() => {
        console.log('begin!');
        window.cable = ActionCable.createConsumer("http://localhost:3001/query_update");
        let channel = window.cable.subscriptions.create({channel: 'QueryChannel', query_id: 1}, {
            received: data => {
                console.log('recieved data from THE socket:',data);
            }
        })
    }, []);

    return (
        <div className="content">
            <h2>Queries for {user.id}|{user.name}</h2>
            <Query name={"first query"} count={6}/>
        </div>
    );
};

export default Content;