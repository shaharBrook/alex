import React from 'react';
import './query.css';

const Query = ({name, count}) => (
    <div className="query">
        <label className="name">{name}</label>
        <label className="count">{count}</label>
    </div>
);

export default Query;