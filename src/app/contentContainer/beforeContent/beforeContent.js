import React, {useContext, useState} from 'react';
import Context from "../../../context/context";
import {Config} from "../../../config/config";
import {saveUser} from "../../../state/state";
const axios = require('axios');

const BeforeContent = () => {
    const {url, port} = Config.server;

    const {setUser} = useContext(Context);
    const [id, setId] = useState('');

    const onIdChange = ({target}) => {
        const id = target.value;
        setId(id);
        axios.get(`${url}:${port}/users/${id}`)
            .then(({data}) => {
                saveUser(data);
                setUser(data);
            })
            .catch(error => {
                console.log(error);
            });
    };

    return (
        <div className="before-content">
            <h2>Enter your id</h2>
            <input type="text" onChange={onIdChange} value={id}/>
        </div>
    );
};

export default BeforeContent;