import React, {useContext} from 'react';
import './contentContainer.css';
import Content from "./content/content";
import BeforeContent from "./beforeContent/beforeContent";
import Context from "../../context/context";

const ContentContainer = () => {
    const {isLoggedIn} = useContext(Context);
    return (
        <div className="content-container">
            {isLoggedIn() ? <Content/>  : <BeforeContent/>}
        </div>
    );
};

export default ContentContainer;