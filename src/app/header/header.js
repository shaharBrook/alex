import React, {useContext} from 'react';
import './header.css';
import Context from "../../context/context";
import {getInitialUser} from "../../context/initialContext";

const Header = () => {
    const {isLoggedIn, setUser} = useContext(Context);

    const logout = () => {
        setUser(getInitialUser());
    };

    return (
        <div className="header">
            <h1>alex</h1>
            {isLoggedIn() && (<button onClick={logout}>logout</button>)}
        </div>
    );
};

export default Header;