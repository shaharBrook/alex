export const saveUser = (user) => {
    localStorage.setItem('user', JSON.stringify(user));
};

export const getUser = (user) => JSON.parse(localStorage.getItem('user'));

export const isUserSaved = () => getUser() && getUser().id;