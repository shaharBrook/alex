var ActionCable = require('actioncable');

(function() {
    this.App || (this.App = {});

    App.cable = ActionCable.createConsumer("ws://cable.example.com");
}).call(this);